
def multiplier3(number):
    if number % 3 == 0:
        return True
    else:
        return False


multiplier_numbers3 = []
starting = 0
while starting <1000:
    if multiplier3(starting):
        multiplier_numbers3.append(starting)
    starting = starting +1
print(f"multiplier of 3:{multiplier_numbers3}")
print(len(multiplier_numbers3))

n = len(multiplier_numbers3)
sum1 = sum(multiplier_numbers3, n)
print(f"multipliers 3 summation:{sum1}")


def multiplier5(number):
    if number % 5 == 0:
        return True
    else:
        return False


multiplier_numbers5 = []
starting = 0
while starting <1000:
    if multiplier5(starting):
        multiplier_numbers5.append(starting)
    starting = starting +1
print(f"multiplier of 5:{multiplier_numbers5}")
print(len(multiplier_numbers5))

n = len(multiplier_numbers5)
sum2 = sum(multiplier_numbers5, n)
print(f"multipliers 5 summation:{sum2}")

def multiplier3_5(number):
    if number % 3 == 0 and number % 5 == 0:
        return True
    else:
        return False


multiplier_numbers35 = []
starting = 0
while starting <1000:
    if multiplier3_5(starting):
        multiplier_numbers35.append(starting)
    starting = starting +1
print(f"multiplier of 3 and 5:{multiplier_numbers35}")
print(len(multiplier_numbers35))

n = len(multiplier_numbers35)
sum3 = sum(multiplier_numbers35, n)
print(f"multipliers  3 and 5 summation:{sum3}")


def multiplier3_5(number):
    if number % 3 == 0 or number % 5 == 0:
        return True
    else:
        return False


multiplier_numbers35 = []
starting = 0
while starting <1000:
    if multiplier3_5(starting):
        multiplier_numbers35.append(starting)
    starting = starting +1
print(f"multiplier of 3 or 5:{multiplier_numbers35}")
print(len(multiplier_numbers35))

n = len(multiplier_numbers35)
sum4 = sum(multiplier_numbers35, n)
print(f"multipliers  3 or 5 summation:{sum4}")

final_sum = sum4 - sum3
print(f"ProjectEuler first answer is:{final_sum}")